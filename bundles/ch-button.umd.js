(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core')) :
    typeof define === 'function' && define.amd ? define('ch-button', ['exports', '@angular/core'], factory) :
    (global = typeof globalThis !== 'undefined' ? globalThis : global || self, factory(global['ch-button'] = {}, global.ng.core));
}(this, (function (exports, i0) { 'use strict';

    var ChButtonService = /** @class */ (function () {
        function ChButtonService() {
        }
        return ChButtonService;
    }());
    ChButtonService.ɵprov = i0.ɵɵdefineInjectable({ factory: function ChButtonService_Factory() { return new ChButtonService(); }, token: ChButtonService, providedIn: "root" });
    ChButtonService.decorators = [
        { type: i0.Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    ChButtonService.ctorParameters = function () { return []; };

    var ChButtonComponent = /** @class */ (function () {
        function ChButtonComponent() {
        }
        ChButtonComponent.prototype.ngOnInit = function () {
        };
        return ChButtonComponent;
    }());
    ChButtonComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'ch-button',
                    template: "\n    <p>\n      ch-button works!\n    </p>\n  "
                },] }
    ];
    ChButtonComponent.ctorParameters = function () { return []; };

    var ChButtonModule = /** @class */ (function () {
        function ChButtonModule() {
        }
        return ChButtonModule;
    }());
    ChButtonModule.decorators = [
        { type: i0.NgModule, args: [{
                    declarations: [ChButtonComponent],
                    imports: [],
                    exports: [ChButtonComponent]
                },] }
    ];

    /*
     * Public API Surface of ch-button
     */

    /**
     * Generated bundle index. Do not edit.
     */

    exports.ChButtonComponent = ChButtonComponent;
    exports.ChButtonModule = ChButtonModule;
    exports.ChButtonService = ChButtonService;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=ch-button.umd.js.map
