import { ɵɵdefineInjectable, Injectable, Component, NgModule } from '@angular/core';

class ChButtonService {
    constructor() { }
}
ChButtonService.ɵprov = ɵɵdefineInjectable({ factory: function ChButtonService_Factory() { return new ChButtonService(); }, token: ChButtonService, providedIn: "root" });
ChButtonService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
ChButtonService.ctorParameters = () => [];

class ChButtonComponent {
    constructor() { }
    ngOnInit() {
    }
}
ChButtonComponent.decorators = [
    { type: Component, args: [{
                selector: 'ch-button',
                template: `
    <p>
      ch-button works!
    </p>
  `
            },] }
];
ChButtonComponent.ctorParameters = () => [];

class ChButtonModule {
}
ChButtonModule.decorators = [
    { type: NgModule, args: [{
                declarations: [ChButtonComponent],
                imports: [],
                exports: [ChButtonComponent]
            },] }
];

/*
 * Public API Surface of ch-button
 */

/**
 * Generated bundle index. Do not edit.
 */

export { ChButtonComponent, ChButtonModule, ChButtonService };
//# sourceMappingURL=ch-button.js.map
